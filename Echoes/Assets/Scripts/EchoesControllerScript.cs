﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class EchoesControllerScript : MonoBehaviour
{

    public List<Card> AllCards = new List<Card>();
    public List<Card> EnemyCards = new List<Card>();
    public List<Card> ActiveDeck = new List<Card>();
    public List<Card> ActiveEnemyDeck = new List<Card>();
    public List<Card> PreviousDeck = new List<Card>();
    public ReadCardFile readCardScript;
    public SignIn signInScript;
    public DeckLogControl deckController;
    public TextLogControl textLog;

    // Start is called before the first frame update
    void Start()
    {
        AllCards = new List<Card>();
        EnemyCards = new List<Card>();
        ActiveDeck = new List<Card>();
        ActiveEnemyDeck = new List<Card>();
        PreviousDeck = new List<Card>();
    }

    public void UpdateCardCollection()
    {
        readCardScript.ReadCardFiles();
        AllCards = new List<Card>();
        EnemyCards = new List<Card>();
        ActiveDeck = new List<Card>();
        ActiveEnemyDeck = new List<Card>();
        PreviousDeck = new List<Card>();
        int count = 0;

        for (int i = 0; i < 30; i++)
        {

            Card newItem = new Card();
            newItem.name = readCardScript.normalCardName[i];
            newItem.description = readCardScript.normalCardDescription[i];
            newItem.influence = int.Parse(readCardScript.normalCardInfluence[i]);
            newItem.powerLevel = int.Parse(readCardScript.normalCardPowerLevel[i]);
            newItem.effect = ParseCardEffect(readCardScript.normalCardEffect[i]);
            newItem.id = int.Parse(readCardScript.normalCardID[i]);
            newItem.image = ImageLoader(newItem.id);
            AllCards.Add(newItem); AllCards.Add(newItem); AllCards.Add(newItem);
            count++;

        }

        Debug.Log("stages " + signInScript.userLogged.userStage);
        Debug.Log("Count" + readCardScript.heroCardName.Count);
        string stages = signInScript.userLogged.userStage.ToString();
        
        if (int.Parse(signInScript.userLogged.userStage) > 0)
        {
            for (int i = 0; i < readCardScript.heroCardName.Count; i++)
            {
                if(stages[i]!='0'){
                    Card newItem = new Card();
                    newItem.name = readCardScript.heroCardName[i];
                    newItem.description = readCardScript.heroCardDescription[i];
                    newItem.influence = int.Parse(readCardScript.heroCardInfluence[i]);
                    newItem.powerLevel = int.Parse(readCardScript.heroCardPowerLevel[i]);
                    newItem.effect = ParseCardEffect(readCardScript.heroCardEffect[i]);
                    newItem.id = int.Parse(readCardScript.heroCardID[i]);
                    newItem.image = ImageLoader(newItem.id);
                    AllCards.Add(newItem); AllCards.Add(newItem); AllCards.Add(newItem);
                    count++;
                }
            }
        }

        if (readCardScript.hasDLC)
        {
            for (int i = 0; i < 10; i++)
            {
                Card newItem = new Card();
                newItem.name = readCardScript.heroDLCCardName[i];
                newItem.description = readCardScript.heroDLCCardDescription[i];
                newItem.influence = int.Parse(readCardScript.heroDLCCardInfluence[i]);
                newItem.powerLevel = int.Parse(readCardScript.heroDLCCardPowerLevel[i]);
                newItem.effect = ParseCardEffect(readCardScript.heroDLCCardEffect[i]);
                newItem.id = int.Parse(readCardScript.heroDLCCardID[i]);
                newItem.image = ImageLoader(newItem.id);
                AllCards.Add(newItem); AllCards.Add(newItem); AllCards.Add(newItem);
                count++;

            }
        }


        for (int i = 0; i < 30; i++)
        {
            Card newItem = new Card();
            newItem.name = readCardScript.normalEnemyCardName[i];
            newItem.description = readCardScript.normalEnemyCardDescription[i];
            newItem.influence = int.Parse(readCardScript.normalEnemyCardInfluence[i]);
            newItem.powerLevel = int.Parse(readCardScript.normalEnemyCardPowerLevel[i]);
            newItem.effect = ParseCardEffect(readCardScript.normalEnemyCardEffect[i]);
            newItem.id = int.Parse(readCardScript.normalEnemyCardID[i]);
            newItem.image = ImageLoader(newItem.id);
            EnemyCards.Add(newItem);
        }
        for (int i = 0; i < 9; i++)
        {
            Card newItem = new Card();
            newItem.name = readCardScript.enemyHeroCardName[i];
            newItem.description = readCardScript.enemyHeroCardDescription[i];
            newItem.influence = int.Parse(readCardScript.enemyHeroCardInfluence[i]);
            newItem.powerLevel = int.Parse(readCardScript.enemyHeroCardPowerLevel[i]);
            newItem.effect = ParseCardEffect(readCardScript.enemyHeroCardEffect[i]);
            newItem.id = int.Parse(readCardScript.enemyHeroCardID[i]);
            newItem.image = ImageLoader(newItem.id);
            EnemyCards.Add(newItem);
        }

        deckController.ResetCards();
        textLog.cleanText();
    }

    public CardEffect ParseCardEffect(string args)
    {
        string[] parts = args.Split(':');
        int effectID = int.Parse(parts[0]);
        int effectModifier = int.Parse(parts[1]);
        CardEffect result;
        switch (effectID)
        {
            case 1:
                result = new ModifyEventInfluence(effectModifier);
                break;
            case 2:
                result = new ModifySelfInfluence(effectModifier);
                break;
            case 3:
                result = new ModifyAllFriendlyCardInfluence(effectModifier);
                break;
            case 4:
                result = new ModifyAllEnemyCardInfluence(effectModifier);
                break;
            case 5:
                result = new DrawCardsEffect(effectModifier);
                break;
            case 6:
                result = new ModifyAdjacentFriendlyCardInfluence(effectModifier);
                break;
            case 7:
                result = new ModifyAdjacentEnemyCard(effectModifier);
                break;
            case 8:
                result = new ModifyRandomFriendlyCardInfluence(effectModifier);
                break;
            case 9:
                result = new ModifyRandomEnemyCardInfluence(effectModifier);
                break;
            default:
                result = null;
                break;
        }

        return result;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void AddCard(int id)
    {
        foreach (Card c in AllCards)
        {
            if (c.id == id)
            {
                Card card = new Card
                {
                    id = c.id,
                    name = c.name,
                    influence = c.influence,
                    image = c.image,
                    powerLevel = c.powerLevel,
                    description = c.description,
                    effect = c.effect
                };

                ActiveDeck.Add(card);
                break;
            }
        }

        Debug.Log("DECK SIZE " + ActiveDeck.Count);
    }

    public void RemoveCard(int id)
    {
        foreach (Card c in ActiveDeck)
        {
            if (c.id == id)
            {
                ActiveDeck.Remove(c);
                break;
            }
        }
        Debug.Log("DECK SIZE " + ActiveDeck.Count);

    }


    Sprite ImageLoader(int id)
    {
        string folderPath = Application.streamingAssetsPath + "/cards/";
        string stringID = string.Format("{0}.png",id);
        byte[] pngBytes = System.IO.File.ReadAllBytes(folderPath+stringID);
        Texture2D tex = new Texture2D(2, 2);
        tex.LoadImage(pngBytes);
        Sprite fromTex = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);
        return fromTex;
    }

    public void ExitGame()
    {
        Application.Quit();
    }

}
