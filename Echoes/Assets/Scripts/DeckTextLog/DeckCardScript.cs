﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DeckCardScript : MonoBehaviour
{
    [SerializeField]
    private string myText;
    
    [SerializeField]
    private Color myColor;
    public Text maybeText;
    public Text cardID;

    
    [SerializeField]
    private TextLogControl logControl;
    public GameObject g_SelectedObject;

    public EchoesControllerScript echoesController;

    public Image Sprite;



    void Start(){
        logControl = GameObject.Find("TextLog").GetComponent<TextLogControl>();
        echoesController = GameObject.FindGameObjectWithTag("EchoesController").GetComponent<EchoesControllerScript>();
    }
    public void SelectUnselectCard(){

        if(g_SelectedObject.activeSelf){
            g_SelectedObject.SetActive(false);
            LogText();
        }else {
            g_SelectedObject.SetActive(true);
            RemoveText();
        }
    }
    public void LogText(){
        logControl.LogText(maybeText.text,myColor);
        echoesController.AddCard(int.Parse(cardID.text));

    }

    public void RemoveText(){
        logControl.RemoveText(maybeText.text);
        echoesController.RemoveCard(int.Parse(cardID.text));
    }
}