﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DeckLogControl : MonoBehaviour
{
    public List<Card> DeckCards = new List<Card>();

    [SerializeField]
    private GameObject cardTemplate;

    [SerializeField]
    private GridLayoutGroup gridGroup;

    public ReadCardFile readCardScript;
    public SignIn signInScript;

    public  GameObject content;
    public EchoesControllerScript echoesController;


   

    void Start()
    {
        ResetCards();
    }

    public void ResetCards()
    {
        for (int i = 1; i <= DeckCards.Count; i++)
        {
            GameObject.Destroy(content.GetComponent<Transform>().GetChild(i).gameObject);
        }

        DeckCards = echoesController.AllCards;
        Debug.Log("DeckCards "+ DeckCards.Count);
        Debug.Log("AllCards " +echoesController.AllCards.Count);


        GenerateCards();
    }

    void GenerateCards()
    {
        if (DeckCards.Count < 4)
        {
            gridGroup.constraintCount = DeckCards.Count;
        }
        else
        {
            gridGroup.constraintCount = 3;
        }

        foreach (Card item in DeckCards)
        {
            GameObject newCard = Instantiate(cardTemplate) as GameObject;
            newCard.SetActive(true);

            Text[] hi = newCard.GetComponentsInChildren<Text>();
            hi[2].text = item.name;
            hi[1].text = string.Format("{0}",item.influence);
            hi[0].text = item.description;
            hi[3].text = string.Format("{0}",item.id);
            newCard.GetComponent<DeckCardScript>().Sprite.sprite = item.image;
            newCard.transform.SetParent(cardTemplate.transform.parent, false);
        }
    }
}
