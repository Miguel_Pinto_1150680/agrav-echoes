﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextLogControl : MonoBehaviour
{
    [SerializeField]
    private GameObject textTemplate;

    private List<GameObject> textItems = new List<GameObject>();
    private Text[] allText;
    public GameObject content;
    void OnEnable()
    {
        textItems = new List<GameObject>();
    }

    public void cleanText()
    {
        for (int i = 1; i <= textItems.Count; i++)
        {
            GameObject.Destroy(content.GetComponent<Transform>().GetChild(i).gameObject);
        }
    }

    public void LogText(string newTextString, Color newColor)
    {


        GameObject newText = Instantiate(textTemplate) as GameObject;
        newText.SetActive(true);

        newText.GetComponent<TextLogItem>().SetText(newTextString, newColor);
        newText.transform.SetParent(textTemplate.transform.parent, false);

        textItems.Add(newText.gameObject);
    }

    public void RemoveText(string newTextString)
    {
        allText = GetComponentsInChildren<Text>();

        foreach (var item in allText)
        {
            if (item.GetComponent<Text>().text == newTextString)
            {

                Destroy(item.gameObject);
                break;
            }
        }

        foreach (var item in textItems)
        {
            if (item.GetComponent<Text>().text == newTextString)
            {
                textItems.Remove(item);
                break;
            }
        }
    }
}
