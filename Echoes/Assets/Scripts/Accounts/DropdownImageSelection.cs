﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class DropdownImageSelection : MonoBehaviour
{

        List<string> items = new List<string>();
        List<Sprite> spriteList = new List<Sprite>();
    public Text Textbox;
    public Image AvatarImage;
    // Start is called before the first frame update
    void Start()
    {
        var dropdown = transform.GetComponent<Dropdown>();

        dropdown.options.Clear();

        //Create Image list
        items.Add("Otter");
        items.Add("Anne Frank");

        spriteList.Add(Resources.Load <Sprite>("Otter"));
        spriteList.Add(Resources.Load <Sprite>("AnneFrank"));
        
        //Fill Image List
        foreach (var item in items){
            dropdown.options.Add(new Dropdown.OptionData() {text = item});
        }

        DropdownItemSelected(dropdown);

        dropdown.onValueChanged.AddListener(delegate {DropdownItemSelected(dropdown);});
    }

    void DropdownItemSelected(Dropdown dropdown){
        int index = dropdown.value;
        // Textbox.text = dropdown.options[index].text;
        
        ChangeImage(index);
    }

    void ChangeImage(int index){
        AvatarImage.sprite = spriteList[index]; 
    }

}
