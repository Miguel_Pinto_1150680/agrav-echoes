﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using FullSerializer;
using System.Text.RegularExpressions;
using Proyecto26;
public class SignIn : MonoBehaviour
{ 
    private string databaseURL = "https://agrav-echoes-default-rtdb.europe-west1.firebasedatabase.app/users"; 
    // private string AuthKey = "secret ;D";
    public string AuthKey = "AIzaSyB0papksovEijhaIV78KZk7gURclBOHXgc";

    public User userLogged;
    public InputField  _signInEmailField;
    public InputField  _signInPasswordField;
    public static string EmailSignIn = "";
    public static string AvatarSignIn = "";
    public static string playerUsername = "";
    public static string PasswordSignIn = "";
    public static fsSerializer serializer = new fsSerializer();
    public StateSign CurrentState {get; private set; }

    private Color colorDefault;
    public Button _signInButton;

    public GameObject signUpPanel;
    public GameObject mainMenuPanel;

    public string idToken;
    public bool logged = false;
    
    public static string localId;

    public string getLocalId;

    private MainMenuScript mainMenuScript;
    void Start()
    {
        colorDefault = new Color32(152,132,98,255);
        _signInEmailField.text = "";
        _signInPasswordField.text = "";
        _signInButton.interactable = false;
    }

    public void LogOut(){
        
        MainMenuScript[] mainMenuScriptList = Resources.FindObjectsOfTypeAll<MainMenuScript>();

        mainMenuScript = mainMenuScriptList[0];

        User u = new User();
        userLogged = u;
        logged = false;
        mainMenuPanel.SetActive(true);
        mainMenuScript.RefreshMethod();
    }

    // Update is called once per frame
    void Update()
    {
        if (!string.IsNullOrEmpty(_signInEmailField.text) && !string.IsNullOrEmpty(_signInPasswordField.text))
        {
            SetStateSign(StateSign.Ok);
        }
        else
        {
            SetStateSign(StateSign.Empty);
            _signInButton.interactable = false;
        }

        if  (!string.IsNullOrEmpty(_signInPasswordField.text) && _signInPasswordField.text.Length<6){
            SetStateSign(StateSign.PasswordShort);
            _signInButton.interactable = false;
        }

        
        if  (!string.IsNullOrEmpty(_signInEmailField.text) && !IsValidEmail(_signInEmailField.text)){
            SetStateSign(StateSign.EmailInvalid);
            _signInButton.interactable = false;
        }


        if (CurrentState.Equals(StateSign.EmailInvalid))
        {
            _signInEmailField.image.color = Color.red;
        } else {
            _signInEmailField.image.color = colorDefault;
        }


        if (CurrentState.Equals(StateSign.PasswordShort))
        {
            _signInPasswordField.image.color = Color.red;
        } else {
            _signInPasswordField.image.color = colorDefault;
        }

        if (CurrentState.Equals(StateSign.Ok))
        {

            _signInButton.interactable = true;
            EmailSignIn = _signInEmailField.text;
            PasswordSignIn = _signInPasswordField.text;

        }

        
    }

    bool IsValidEmail(string email)
    {
        Regex rgx = new Regex(@"^([a-zA-Z0-9+_.-]*)@([a-zA-Z0-9+_.-]*)\.([a-zA-Z0-9+_-]{2,})$");
        return rgx.IsMatch(email);
    }

    private void SetStateSign(StateSign state){
            CurrentState = state;
        // OnStateChanged.Invoke(state);
    }

    public enum StateSign{
        Empty,
        EmailInvalid,
        PasswordShort,
        Ok
    }

    public void SignInUserButton(){
        SignInUser();
    }

    private void SignInUser()
    {
        // userLogged = new User();
        // userLogged.localId = "";
        string userData = "{\"email\":\"" + EmailSignIn + "\",\"password\":\"" + PasswordSignIn + "\",\"returnSecureToken\":true}";
        
        RestClient.Post<SignResponse>("https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=" + AuthKey, userData).Then(
            response =>
            {

                idToken = response.idToken;
                localId = response.localId;
                logged = true;
                GetUsername();
            }).Catch(error =>
        {
            Debug.LogError(error);
        });
    }

    private void RetrieveFromDatabase()
    {
        RestClient.Get<User>(databaseURL + "/" + getLocalId + ".json?auth=" + idToken).Then(response =>
            {
                userLogged = response;
                Debug.Log(userLogged.userEmail);
                Debug.Log(userLogged.userName);
                signUpPanel.SetActive(false);
                mainMenuPanel.SetActive(true);
            });

            
    }

    private void GetUsername()
    {
        RestClient.Get<User>(databaseURL + "/" + localId + ".json?auth=" + idToken).Then(response =>
        {
            playerUsername = response.userName;
        });
        GetLocalId();
    }

    private void GetLocalId(){
        RestClient.Get(databaseURL + ".json?auth=" + idToken).Then(response =>
        {
            var username = playerUsername;
            
            fsData userData = fsJsonParser.Parse(response.Text);
            Dictionary<string, User> users = null;
            serializer.TryDeserialize(userData, ref users);

            foreach (var user in users.Values)
            {
                if (user.userName == username)
                {
                    getLocalId = user.localId;
                    RetrieveFromDatabase();
                    break;
                }
            }
        }).Catch(error =>
        {
            Debug.Log(error);
        });
    }
}
