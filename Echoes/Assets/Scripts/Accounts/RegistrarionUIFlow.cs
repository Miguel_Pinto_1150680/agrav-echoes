using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using FullSerializer;
using Proyecto26;
using System.Text.RegularExpressions;

public class RegistrarionUIFlow : MonoBehaviour
{
    private string databaseURL = "https://agrav-echoes-default-rtdb.europe-west1.firebasedatabase.app/users";
    // private string AuthKey = "secret ;D";
    public string AuthKey = "AIzaSyB0papksovEijhaIV78KZk7gURclBOHXgc";
    // public static fsSerializer serializer = new fsSerializer();


    public InputField _emailField;
    public InputField _passwordField;
    public InputField _verifyPasswordField;
    public InputField _usernameField;

    public Dropdown _avatarField;

    public Toggle _eulaToggle;

    public static string Email = "";
    public static string Username = "";
    public static string Password = "";
    public static string Avatar = "";
    User user1 = new User();
    private string idToken;

    public static string localId;

    private string getLocalId;

    public Button _signUpButton;
    private Coroutine _signUpCoroutine;

    public State CurrentState { get; private set; }

    private Color colorDefault;

    void Start()
    {
        ComputeState();
        _signUpButton.interactable = false;
        colorDefault = new Color32(152, 132, 98, 255);
    }

    void Update()
    {

        if (!string.IsNullOrEmpty(_emailField.text) && !string.IsNullOrEmpty(_passwordField.text) && !string.IsNullOrEmpty(_verifyPasswordField.text) && !string.IsNullOrEmpty(_usernameField.text))
        {
            ComputeState();
        }
        else
        {
            SetState(State.Empty);
            _signUpButton.interactable = false;
        }
        if (!string.IsNullOrEmpty(_passwordField.text) && _passwordField.text.Length < 6)
        {
            SetState(State.PasswordShort);
            _signUpButton.interactable = false;
        }


        if (!string.IsNullOrEmpty(_emailField.text) && !IsValidEmail(_emailField.text))
        {

            SetState(State.EmailInvalid);
            _signUpButton.interactable = false;
        }

        if (string.IsNullOrEmpty(_usernameField.text) && CurrentState == State.Ok)
        {
            SetState(State.UsernameInvalid);
            _signUpButton.interactable = false;
        }


        if (CurrentState.Equals(State.EmailInvalid))
        {
            _emailField.image.color = Color.red;
        }
        else
        {
            _emailField.image.color = colorDefault;
        }

        if (CurrentState.Equals(State.UsernameInvalid))
        {
            _usernameField.image.color = Color.red;
        }
        else
        {
            _usernameField.image.color = colorDefault;
        }

        if (CurrentState.Equals(State.PasswordShort))
        {
            _passwordField.image.color = Color.red;
        }
        else
        {
            _passwordField.image.color = colorDefault;
        }

        if (CurrentState.Equals(State.Empty))
        {
            _signUpButton.interactable = false;
            //Dizer ao user que os campos tem de estar todos preenchidos
        }

        if (CurrentState.Equals(State.Ok))
        {

            if (!_eulaToggle.isOn)
            {
                _signUpButton.interactable = false;
            }
            else
            {
                _verifyPasswordField.image.color = colorDefault;

                _signUpButton.interactable = true;
                Email = _emailField.text;
                Password = _passwordField.text;
                Username = _usernameField.text;

                int index = _avatarField.value;
                Avatar = _avatarField.options[index].text;
            }
        }

    }

    bool IsValidEmail(string email)
    {
        Regex rgx = new Regex(@"^([a-zA-Z0-9+_.-]*)@([a-zA-Z0-9+_.-]*)\.([a-zA-Z0-9+_-]{2,})$");
        return rgx.IsMatch(email);
    }


    private void HandValueChanged(string _)
    {
        ComputeState();
    }

    public void ComputeState()
    {

        if (_passwordField.text != _verifyPasswordField.text)
        {
            SetState(State.PasswordDontMatch);

            _signUpButton.interactable = false;
            _verifyPasswordField.image.color = Color.red;
        }
        else if (string.IsNullOrEmpty(_usernameField.text))
        {
            _signUpButton.interactable = false;
            _usernameField.image.color = Color.red;
        }
        else
        {
            SetState(State.Ok);
            _usernameField.image.color = colorDefault;
            _verifyPasswordField.image.color = colorDefault;
        }
    }

    private void SetState(State state)
    {
        CurrentState = state;
    }

    public enum State
    {
        Empty,
        EmailInvalid,
        UsernameInvalid,
        PasswordShort,
        PasswordDontMatch,
        Ok
    }

    public void SignUpUserButton()
    {
        int index = _avatarField.value;
        SignUpUser(_emailField.text, _passwordField.text, _usernameField.text, _avatarField.options[index].text);
    }

    public void CreateAccount(bool INIT)
    {
        User user = new User();
        if (INIT)
        {
            user.userEmail = Email;
            user.userName = Username;
            user.userAvatar = Avatar;
            user.userRating = 0;
            user.userStage = "000000000";
        }
        RestClient.Put(databaseURL + "/" + localId + ".json?auth=" + idToken, user);
    }

    private void RetrieveFromDatabase()
    {
        RestClient.Get<User>(databaseURL + Email + ".json").Then(response =>
            {
                user1 = response;
            });
    }

    private void SignUpUser(string newEmail, string newPassword, string username, string avatar)
    {
        RequestHelper currentRequest = new RequestHelper
        {
            Uri = "https://identitytoolkit.googleapis.com/v1/accounts:signUp",
            Params = new Dictionary<string, string> {
                { "key", AuthKey }
            },
            Body = new NewUserRegister
            {
                email = newEmail,
                password = newPassword,
                returnSecureToken = true
            },
            EnableDebug = true
        };

        RestClient.Post<SignResponse>(currentRequest).Then(
            response =>
            {
                Debug.Log("Get the Response");
                idToken = response.idToken;
                localId = response.localId;
                Username = username;
                Avatar = avatar;
                CreateAccount(true);

            }).Catch(error =>
            {
                Debug.Log(error);
            });
    }



}
