﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class User
{
    public string userName;
    public string userEmail;
    public int userRating;
    public string userStage;
    public string userAvatar;
    public string localId;
    
    public User()
    {
        userName = "";
        userEmail = "";
        userStage = "";
        userAvatar = "";
        localId = RegistrarionUIFlow.localId;
    }
}