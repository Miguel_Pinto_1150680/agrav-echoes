﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using FullSerializer;
using System.Text.RegularExpressions;
using Proyecto26;

public class ProfileScript : MonoBehaviour
{
    private string databaseURL = "https://agrav-echoes-default-rtdb.europe-west1.firebasedatabase.app/users"; 
    // private string AuthKey = "secret ;D";
    public string AuthKey = "AIzaSyB0papksovEijhaIV78KZk7gURclBOHXgc";
    private SignIn signInScript;
    public Image _profileImage;
    public InputField _usernameText;
    public InputField _emailText;
    public Dropdown dropdown;
    private string newUsername;
    private string newAvatar;
    public Button _saveButton;
    private int index;
    List<Sprite> spriteList = new List<Sprite>();
    List<string> items = new List<string>();
    public User newUSer;
    public GameObject LoginPanelWithScript;

    
    void OnEnable()
    {
        spriteList = new List<Sprite>();
        items = new List<string>();

        items.Add("Otter");
        items.Add("Anne Frank");
        spriteList.Add(Resources.Load <Sprite>("Otter"));
        spriteList.Add(Resources.Load <Sprite>("AnneFrank"));

        RefreshMethod();
    }

    public void CheckForchanges(){
        if (!string.IsNullOrEmpty(_usernameText.text) && !signInScript.userLogged.userName.Equals(newUsername)){
            _saveButton.interactable = true;
        } else if(newAvatar!=signInScript.userLogged.userAvatar){
            _saveButton.interactable = true;
        } else {
            _saveButton.interactable = false;
        }
    }

    public void RefreshName(){
        newUsername = _usernameText.text;
        CheckForchanges();
    }

    public void RefreshAvatar(){
        int index = dropdown.value;
        newAvatar = items[index];
        CheckForchanges();
    }
    
    public void RefreshMethod(){

        signInScript = LoginPanelWithScript.GetComponent<SignIn>();

        var count = 0;
        foreach (var item in dropdown.options){

            if (item.text.Equals(signInScript.userLogged.userAvatar)){
                index = count;
            }
            count++;
        }

        dropdown.value = index;

        if(signInScript != null)
        {
            _profileImage.sprite = Resources.Load<Sprite>(Regex.Replace(signInScript.userLogged.userAvatar, @"\s+", ""));
            _usernameText.text = signInScript.userLogged.userName; 
            _emailText.text = signInScript.userLogged.userEmail;

            newUsername = signInScript.userLogged.userName;
            newUSer = signInScript.userLogged;
        }
    }

    public void SaveMethod(){
        newUSer.userName = newUsername;
        newUSer.userAvatar = newAvatar;

         RestClient.Put<User>(databaseURL + "/" + signInScript.getLocalId + ".json?auth=" + signInScript.idToken,newUSer).Then(response =>
            {
                Debug.Log(signInScript.userLogged.userEmail);
                Debug.Log(signInScript.userLogged.userName);
                Debug.Log(signInScript.userLogged.userAvatar);
                Debug.Log("UPDATED");
            });
    }
}