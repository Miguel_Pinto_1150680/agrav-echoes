﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text.RegularExpressions;

public class MainMenuScript : MonoBehaviour
{
    public GameObject _mainMenuLoginButton;
    public GameObject _mainMenuSignUpButton;
    public GameObject _mainMenuLogOutButton;
    public GameObject _mainMenuProfileButton;
    public Image _mainMenuImage;
    public GameObject _mainMenuUsername;
    public Text _usernameText;
    private SignIn signInScript;
    public GameObject LoginPanelWithScript;

    public Button PlayButton;
    public Button StageEditorButton;
    public Button DLCButton;
    public Button RankingsButton;
    public EchoesControllerScript echoesController;

    private bool setCollection = false;

    void OnEnable()
    {
        RefreshMethod();
    }

    public void RefreshMethod()
    {
        signInScript = LoginPanelWithScript.GetComponent<SignIn>();

        if (signInScript != null)
        {
            if (signInScript.logged)
            {
                
                _mainMenuImage.sprite = Resources.Load<Sprite>(Regex.Replace(signInScript.userLogged.userAvatar, @"\s+", ""));
                _usernameText.text = signInScript.userLogged.userName;
                _mainMenuLoginButton.SetActive(false);
                _mainMenuSignUpButton.SetActive(false);
                _mainMenuLogOutButton.SetActive(true);
                _mainMenuUsername.SetActive(true);
                _mainMenuProfileButton.SetActive(true);
                if (!setCollection)
                {
                    echoesController.UpdateCardCollection();
                    setCollection = true;
                }
                PlayButton.interactable = true;
                StageEditorButton.interactable = true;
                DLCButton.interactable = true;
                RankingsButton.interactable = true;


            }
            else
            {
                _mainMenuImage.sprite = Resources.Load<Sprite>("default_avatar");
                setCollection = false;
                _mainMenuLoginButton.SetActive(true);
                _mainMenuSignUpButton.SetActive(true);
                _mainMenuLogOutButton.SetActive(false);
                _mainMenuUsername.SetActive(false);
                _mainMenuProfileButton.SetActive(false);
                PlayButton.interactable = false;
                StageEditorButton.interactable = false;
                DLCButton.interactable = false;
                RankingsButton.interactable = false;
            }
        }
    }
}
