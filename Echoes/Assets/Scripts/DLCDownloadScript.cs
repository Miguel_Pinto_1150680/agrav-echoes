﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.IO;
using UnityEditor;

public class DLCDownloadScript : MonoBehaviour
{
    public Button thisButton;
    public EchoesControllerScript echoesController;
    public DeckLogControl deckLog;

    void OnEnable()
    {

        string path = "DLCHeroCards.txt";
        if (File.Exists(Application.streamingAssetsPath + "/" + path))
        {
            thisButton.interactable = false;
        }
        else
        {
            Debug.Log("DLC NOT DOWNLOADED");
            thisButton.interactable = true;
        }
    }

    public void StartDownload()
    {
        StartCoroutine(DownloadFile());
    }

    IEnumerator DownloadFile()
    {
        var uwr = new UnityWebRequest("https://raw.githubusercontent.com/Sherinka19/AGRAV/main/DLCHeroCards.txt", UnityWebRequest.kHttpVerbGET);
        string path = Application.streamingAssetsPath + "/DLCHeroCards.txt";
        Debug.Log(path);
        uwr.downloadHandler = new DownloadHandlerFile(path);
        yield return uwr.SendWebRequest();
        if (uwr.isNetworkError || uwr.isHttpError)
        {
            Debug.LogError(uwr.error);
        }
        else
        {
            Debug.Log("File successfully downloaded and saved to " + path);
            thisButton.interactable = false;
            echoesController.UpdateCardCollection();
            deckLog.ResetCards();
        }
    }
}
