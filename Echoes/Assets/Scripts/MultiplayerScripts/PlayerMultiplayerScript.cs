﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using UnityEngine.UI;

public class PlayerMultiplayerScript : NetworkBehaviour
{
    [SyncVar]
    public string playerName;
    [SyncVar]
    public int playerAvatarID;
    public Text playerNameTextBox;
    public Image playerAvatar;
    [SyncVar]
    private int turnOrder;
    [SyncVar]
    public GameObject playerSpawnPos;
    //[SyncVar]
    //public GameObject rightPlayerSpawn;
    // Start is called before the first frame update
    void Start()
    {
        bool bParentFound;

        //Normally player prefabs are instantiated at the root scene level on the client. This is a problem when dealing with game objects that should be located
        //inside a canvas in order to even be visible, so we must reparent the client player prefabs before doing anything else.
        if(isClient)
        {
            bParentFound = false; 
            if(this.transform.parent)
            {
                if (this.transform.parent.tag == "NetworkManager")
                {
                    bParentFound = true;
                    Debug.Log("[FLAG] Found parent");
                }
                else
                {
                    bParentFound = false;
                    Debug.Log("[FLAG] Found no parent");
                }
            }

            if(!bParentFound)
            {
                GameObject networkCanvas = GameObject.FindGameObjectWithTag("NetworkManager");

                Debug.Log("[FLAG] NetworkCanvas object is named" + networkCanvas.name);
                if (networkCanvas)
                {
                    this.transform.parent = networkCanvas.transform;
                    this.transform.localScale = new Vector3(1, 1, 1);
                }
            }
            //We need to update our own prefab with the convenient information about our name and avatar
            //Then, if we're the second player to be created and we're the client, the network manager will 
            //take care of providing us with the relevant information to update the information about the player that was
            //already here
            //Net id 1 is always the host player prefab
            if(netId == 1)
            {
                SetPlayerInfo("HostPlayer", 1);

                this.transform.position = GameObject.Find("LeftPlayerSpawn").transform.position;
                if(isLocalPlayer)
                {
                    Debug.Log("[FLAG] This client is also the host, and is creating the host player prefab");
                }
                else
                {
                    Debug.Log("[FLAG] This client is not the host, but is creating the host player prefab ");
                }

            }
            //Any other netID means this is the client player
            else
            {
                SetPlayerInfo("ClientPlayer", 2);
                this.transform.position = GameObject.Find("RightPlayerSpawn").transform.position;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetPlayerInfo(string _playerName, int _playerAvatarID)
    {
        playerName = _playerName;
        playerAvatarID = _playerAvatarID;

        PlayerUpdateScript script = GameObject.Find("ScriptHolderSingleton").GetComponent<PlayerUpdateScript>();

        //TODO: Currently the names are hardcoded, and so are the avatars used. This will change.
        playerNameTextBox.text = _playerName;
        //Host player
        if (_playerAvatarID == 1)
        {
            playerAvatar.sprite = script.playerAvatarHost;

        }
        //Client player
        else
        {
            playerAvatar.sprite = script.playerAvatarClient;
        }
    }

    public void SetTurnOrder(int _turnOrder)
    {
        turnOrder = _turnOrder;
    }

    public void SetSpawnPos(GameObject _playerSpawnPos)
    {
        playerSpawnPos = _playerSpawnPos;
        Debug.Log("[FLAG] playerSpawnPos is " + _playerSpawnPos.name);
        Debug.Log("[FLAG] before pos is " + this.transform.position);
        this.transform.position = _playerSpawnPos.transform.position;
        Debug.Log("[FLAG] After pos is " + this.transform.position);
    }
}
