﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class SpawnTransformUpdateScript : NetworkBehaviour
{
    // Start is called before the first frame update

    private void Start()
    {
        bool bParentFound;

        //Normally player prefabs are instantiated at the root scene level on the client. This is a problem when dealing with game objects that should be located
        //inside a canvas in order to even be visible, so we must reparent the client player prefabs before doing anything else.
        if (isClient)
        {
            bParentFound = false;
            if (this.transform.parent)
            {
                if (this.transform.parent.tag == "NetworkManager")
                {
                    bParentFound = true;
                    Debug.Log("[FLAG] Found parent");
                }
                else
                {
                    bParentFound = false;
                    Debug.Log("[FLAG] Found no parent");
                }
            }

            if (!bParentFound)
            {
                GameObject networkCanvas = GameObject.FindGameObjectWithTag("NetworkManager");

                Debug.Log("[FLAG] NetworkCanvas object is named" + networkCanvas.name);
                if (networkCanvas)
                {
                    this.transform.parent = networkCanvas.transform;
                    this.gameObject.transform.localPosition = new Vector3(0, 0, 0);
                    this.transform.localScale = new Vector3(1, 1, 1);
                }
            }
        }
    }
}
