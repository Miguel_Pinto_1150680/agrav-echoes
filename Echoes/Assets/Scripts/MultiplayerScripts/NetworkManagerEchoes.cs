﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using UnityEngine.UI;

[AddComponentMenu("")]
public class NetworkManagerEchoes : NetworkManager
{
    public Transform leftPlayerSpawn;
    public Transform rightPlayerSpawn;
    GameObject gameControllerMultiplayer;
    GameObject mpBattlefield;
    // Start is called before the first frame update
    public override void OnServerAddPlayer(NetworkConnection conn)
    {
        // add player at correct spawn position
        Transform start = numPlayers == 0 ? leftPlayerSpawn : rightPlayerSpawn;
        Transform canvas = this.gameObject.transform;
        GameObject player = Instantiate(playerPrefab, start.position, start.rotation,this.gameObject.transform);
        NetworkServer.AddPlayerForConnection(conn, player);

        player.GetComponent<PlayerMultiplayerScript>().SetTurnOrder(numPlayers);

        // If two players are present
        if (numPlayers == 2)
        {
            //Need to show the game field and spawn the enemy players
            gameControllerMultiplayer = Instantiate(spawnPrefabs.Find(prefab => prefab.name == "MultiplayerGameController"),this.gameObject.transform.parent);
            NetworkServer.Spawn(gameControllerMultiplayer);
            GameObject panel = GameObject.FindGameObjectWithTag("NetworkManager");
            mpBattlefield = Instantiate(spawnPrefabs.Find(prefab => prefab.name == "MPBattlefield"), panel.transform);
            NetworkServer.Spawn(mpBattlefield);

            //Need to trigger start/resume game functions
        }
    }

    public override void OnServerDisconnect(NetworkConnection conn)
    {
        base.OnServerDisconnect(conn);
        if (numPlayers == 1)
        {
            //TODO: Need to stop the game, to give the player that disconnected a chance to reconnect
            //OnServerPauseGame()
        }
        else
        {

        }
    }



    
}
