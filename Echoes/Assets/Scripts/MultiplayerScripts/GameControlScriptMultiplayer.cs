﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Mirror;

public class GameControlScriptMultiplayer : NetworkBehaviour
{
    // Start is called before the first frame update

    public GameObject[] HostPlayerCardSpaces;
    public GameObject[] ClientPlayerCardSpaces;
    public GameObject[] Enemy1CardSpaces;
    public GameObject[] Enemy2CardSpaces;
    public Card CardToBePlayed;
    public GameObject[] PlayerHandGameObjects;
    public List<Card> playerHand;
    public List<Card> playerDeck;
    public bool isEnemyTurn = false;
    public bool PlaceMode = false;
    public GameObject WarningPanel;
    public GameObject EndTurnButton;
    private bool isEndOfTurn = false;
    public GameObject DuelInfo;
    public int turns = 15;
    public EchoesControllerScript echoesController;

    //Runtime vars that need to be fetched when this is spawned by the server

    private GameObject enemy1;
    private GameObject enemy2;

    private void OnEnable()
    {
        //SetupPlayerHand();
        //UpdateDuelInfo();
    }
    void Start()
    {
        bool bParentFound;

        bParentFound = false;

        if(this.transform.parent)
        {
            if (this.transform.parent.tag == "NetworkManager")
            {
                bParentFound = true;
            }
            else
            {
                bParentFound = false;
            }
        }

        if (!bParentFound)
        {
            GameObject mpGameFieldCanvas = GameObject.FindGameObjectWithTag("NetworkManager");

            if (mpGameFieldCanvas)
            {
                this.transform.parent = mpGameFieldCanvas.transform;
            }
        }

    }

    public void DoEnemyTurn()
    {
        ToggleControls(false);
        isEnemyTurn = true;
        GetComponent<EnemyController>().DoTurn();
        isEnemyTurn = false;
    }

    public void UpdateDuelInfo()
    {
        int influence = GetComponent<EnemyController>().EnemyInfluence;
        if (influence < 0)
        {
            influence = 0;
        }
        DuelInfo.GetComponent<Text>().text = string.Format("Enemy Influence: {0}\nTurns Left: {1}", influence, turns);
    }

    public void SetupPlayerHand()
    {
        //playerDeck = new List<Card>();
        playerDeck = GameObject.FindGameObjectWithTag("EchoesController").GetComponent<EchoesControllerScript>().ActiveDeck;
        Debug.Log("ACTIVE DECK SIZE " + playerDeck.Count);
        playerHand = new List<Card>();
        /*for (int i = 0; i < 30; i++)
        {
            playerDeck.Add(new Card(i, name: string.Format("card {0}", i), description: string.Format("Super awesome description {0}", i), influence: i, imagePath: "cards/noCard", i, new DrawCardsEffect(2)));
        }*/


        for (int i = 0; i < 3; i++)
        {
            DrawCard();
        }

        DrawCard();
        UpdatePlayerHand();

    }

    public void DrawCard()
    {
        if (playerDeck.Count <= 0)
        {
            return;
        }
        int CardIndex = Random.Range(0, playerDeck.Count);
        Card DrawnCard = playerDeck[CardIndex];
        playerHand.Add(DrawnCard);
        playerDeck.RemoveAt(CardIndex);
        Debug.Log("Player Deck Size " + playerDeck.Count);
    }

    public void UpdatePlayerHand()
    {
        for (int i = 0; i < PlayerHandGameObjects.Length; i++)
        {
            PlayableCardScript playableCardScript = PlayerHandGameObjects[i].GetComponent<PlayableCardScript>();
            if (playableCardScript.bIsSelected)
            {
                PlayerHandGameObjects[i].GetComponent<PlayableCardScript>().UnselectSelf();
            }

            if (i < playerHand.Count && playerHand[i] != null)
            {
                PlayerHandGameObjects[i].SetActive(true);
                PlayerHandGameObjects[i].GetComponent<PlayableCardScript>().card = playerHand[i];
                PlayerHandGameObjects[i].GetComponent<CardInfoScript>().UpdateCard(playerHand[i]);
            }
            else
            {
                PlayerHandGameObjects[i].GetComponent<PlayableCardScript>().card = null;
                PlayerHandGameObjects[i].SetActive(false);
            }
        }

        if (isEndOfTurn)
        {
            if (playerHand.Count <= 5)
            {
                EndTurnButton.GetComponent<Button>().interactable = true;
            }
        }
    }

    public void RemoveCardFromHand(Card card)
    {
        playerHand.Remove(card);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void TogglePlaceMode()
    {
        PlaceMode = !PlaceMode;
    }

    public void TogglePlaceMode(bool value)
    {
        PlaceMode = value;
    }

    public IEnumerator DoCardBattleWithPause(int seconds, List<Card> playerCards, List<Card> enemyCards, int position)
    {
        // doing something

        // waits 2 seconds

        int i = position;
        Card playerCard = playerCards[i];
        Card enemyCard = enemyCards[i];

        if (playerCard != null)
        {
            yield return new WaitForSeconds(seconds);
            Debug.Log("STARTED BATTLE " + position);
        }
        else
        {
            yield return new WaitForSeconds(0);
            Debug.Log("SKIPPED BATTLE");
        }

        if (playerCard != null && enemyCard != null)
        {
            int playerCardInitialInfluence = playerCard.influence;
            int enemyCardInitialInfluence = enemyCard.influence;

            Debug.Log(string.Format("Player used card {0} with {1} influence.\n Enemy used card {2} with {3} influence.", playerCard.name, playerCard.influence, enemyCard.name, enemyCard.influence));
            playerCard.influence -= enemyCardInitialInfluence;
            enemyCard.influence -= playerCardInitialInfluence;


            if (enemyCard.influence <= 0)
            {
                Enemy1CardSpaces[i].GetComponent<CardFrameController>().Card = null;
                Enemy1CardSpaces[i].GetComponent<CardFrameController>().FramedCardObject.SetActive(false);
            }
            else
            {
                Enemy1CardSpaces[i].GetComponent<CardFrameController>().Card = enemyCard;
                Enemy1CardSpaces[i].GetComponent<CardFrameController>().FramedCardObject.GetComponent<CardInfoScript>().UpdateCard(enemyCard);
            }

            if (playerCard.influence <= 0)
            {
                Enemy1CardSpaces[i].GetComponent<CardFrameController>().Card = null;
                Enemy1CardSpaces[i].GetComponent<CardFrameController>().FramedCardObject.SetActive(false);
            }
            else
            {
                Enemy1CardSpaces[i].GetComponent<CardFrameController>().Card = playerCard;
                Enemy1CardSpaces[i].GetComponent<CardFrameController>().FramedCardObject.GetComponent<CardInfoScript>().UpdateCard(playerCard);
            }
        }
        else if (playerCard != null && enemyCard == null)
        {
            GetComponent<EnemyController>().EnemyInfluence -= playerCard.influence;
            UpdateDuelInfo();
        }

        if (position < 2)
        {
            StartCoroutine(DoCardBattleWithPause(seconds, playerCards, enemyCards, i + 1));
        }
        else
        {
            FinishTurnSet();
        }

    }

    public void FinishTurnSet()
    {
        turns--;
        UpdateDuelInfo();

        if (!CheckGameOverConditions())
        {
            ToggleControls(true);
            CheckTurnStartEffects(HostPlayerCardSpaces);
            ValidateFieldedCards();
            DrawCard();
            UpdatePlayerHand();
        }
    }

    public IEnumerator DoBattlePhase()
    {
        Debug.Log("STARTED BATTLE PHASE");
        List<Card> FieldedPlayerCards = new List<Card>();

        foreach (GameObject cardFrame in HostPlayerCardSpaces)
        {
            Card card = cardFrame.GetComponent<CardFrameController>().Card;
            if (card != null)
            {
                FieldedPlayerCards.Add(card);
            }
            else
            {
                FieldedPlayerCards.Add(null);
            }
        }

        List<Card> FieldedEnemyCards = new List<Card>();

        foreach (GameObject cardFrame in Enemy1CardSpaces)
        {
            Card card = cardFrame.GetComponent<CardFrameController>().Card;
            if (card != null)
            {
                FieldedEnemyCards.Add(card);
            }
            else
            {
                FieldedEnemyCards.Add(null);
            }
        }

        yield return StartCoroutine(DoCardBattleWithPause(2, FieldedPlayerCards, FieldedEnemyCards, 0));
    }


    public void CheckTurnEndEffects(GameObject[] cardSpaces)
    {
        int pos = 0;
        foreach (GameObject cardFrame in cardSpaces)
        {
            Card card = cardFrame.GetComponent<CardFrameController>().Card;
            if (card != null && card.effect != null && card.effect.isTriggeredOnTurnEnd())
            {
                string[] args = { string.Format("{0}", pos) };
                card.effect.ApplyEffect(args);
            }
            pos++;
        }
    }

    public void CheckTurnStartEffects(GameObject[] cardSpaces)
    {
        foreach (GameObject cardFrame in cardSpaces)
        {
            Card card = cardFrame.GetComponent<CardFrameController>().Card;
            if (card != null && card.effect != null && card.effect.isTriggeredOnTurnStart())
            {
                string[] args = { };
                card.effect.ApplyEffect(args);
            }
        }
    }

    public void EndTurn()
    {
        if (!CheckGameOverConditions())
        {
            if (playerHand.Count > 5)
            {
                WarningPanel.SetActive(true);
                EndTurnButton.GetComponent<Button>().interactable = false;
                isEndOfTurn = true;
                return;
            }
            isEndOfTurn = false;
            WarningPanel.SetActive(false);
            CheckTurnEndEffects(HostPlayerCardSpaces);
            ValidateFieldedCards();
            DoEnemyTurn();
        }
    }


    public void ToggleControls(bool value)
    {
        foreach (GameObject gameObject in PlayerHandGameObjects)
        {
            gameObject.GetComponent<Button>().interactable = value;
            EndTurnButton.GetComponent<Button>().interactable = value;
        }
    }

    public bool CheckGameOverConditions()
    {
        if ((playerHand.Count < 1 && playerDeck.Count < 1) || turns < 1 || GetComponent<EnemyController>().EnemyInfluence < 1)
        {
            Debug.Log("GAME OVER");
            return true;
        }
        return false;
    }


    public void ValidateFieldedCards()
    {
        List<Card> FieldedPlayerCards = new List<Card>();

        foreach (GameObject cardFrame in HostPlayerCardSpaces)
        {
            Card card = cardFrame.GetComponent<CardFrameController>().Card;
            if (card != null)
            {
                FieldedPlayerCards.Add(card);
            }
            else
            {
                FieldedPlayerCards.Add(null);
            }
        }

        List<Card> FieldedEnemyCards = new List<Card>();

        foreach (GameObject cardFrame in Enemy1CardSpaces)
        {
            Card card = cardFrame.GetComponent<CardFrameController>().Card;
            if (card != null)
            {
                FieldedEnemyCards.Add(card);
            }
            else
            {
                FieldedEnemyCards.Add(null);
            }
        }

        for (int i = 0; i < 3; i++)
        {
            Card playerCard = FieldedPlayerCards[i];
            Card enemyCard = FieldedEnemyCards[i];

            if (enemyCard != null && enemyCard.influence <= 0)
            {
                Enemy1CardSpaces[i].GetComponent<CardFrameController>().Card = null;
                Enemy1CardSpaces[i].GetComponent<CardFrameController>().FramedCardObject.SetActive(false);
            }

            if (playerCard != null && playerCard.influence <= 0)
            {
                HostPlayerCardSpaces[i].GetComponent<CardFrameController>().Card = null;
                HostPlayerCardSpaces[i].GetComponent<CardFrameController>().FramedCardObject.SetActive(false);
            }
        }


    }

}
