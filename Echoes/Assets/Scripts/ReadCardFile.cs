﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEditor;

public class ReadCardFile : MonoBehaviour
{
    //Normal Cards
    public List<string> normalCardName;
    public List<string> normalCardInfluence;
    public List<string> normalCardDescription;
    public List<string> normalCardPowerLevel;
    public List<string> normalCardEffect;
    public List<string> normalCardID;

    //Enemy Normal Cards
    public List<string> normalEnemyCardName;
    public List<string> normalEnemyCardInfluence;
    public List<string> normalEnemyCardDescription;
    public List<string> normalEnemyCardPowerLevel;
    public List<string> normalEnemyCardEffect;
    public List<string> normalEnemyCardID;


    //Hero Cards
    public List<string> heroCardName;
    public List<string> heroCardInfluence;
    public List<string> heroCardDescription;
    public List<string> heroCardPowerLevel;
    public List<string> heroCardEffect;
    public List<string> heroCardID;

    //Enemy Hero Cards
    public List<string> enemyHeroCardName;
    public List<string> enemyHeroCardInfluence;
    public List<string> enemyHeroCardDescription;
    public List<string> enemyHeroCardPowerLevel;
    public List<string> enemyHeroCardEffect;
    public List<string> enemyHeroCardID;

    //DLC Hero Cards
    public List<string> heroDLCCardName;
    public List<string> heroDLCCardInfluence;
    public List<string> heroDLCCardDescription;
    public List<string> heroDLCCardPowerLevel;
    public List<string> heroDLCCardEffect;
    public List<string> heroDLCCardID;


    public bool hasDLC;

    void Start()
    {
        ReadCardFiles();
    }

    public void ReadCardFiles()
    {
        hasDLC = false;

        //Normal Cards
        normalCardName = new List<string>();
        normalCardInfluence = new List<string>();
        normalCardDescription = new List<string>();
        normalCardPowerLevel = new List<string>();
        normalCardEffect = new List<string>();
        normalCardID = new List<string>();

        //Enemy Normal Cards
        normalEnemyCardName = new List<string>();
        normalEnemyCardInfluence = new List<string>();
        normalEnemyCardDescription = new List<string>();
        normalEnemyCardPowerLevel = new List<string>();
        normalEnemyCardEffect = new List<string>();
        normalEnemyCardID = new List<string>();

        //Hero Cards
        heroCardName = new List<string>();
        heroCardInfluence = new List<string>();
        heroCardDescription = new List<string>();
        heroCardPowerLevel = new List<string>();
        heroCardEffect = new List<string>();
        heroCardID = new List<string>();

        //Enemy Hero Cards
        enemyHeroCardName = new List<string>();
        enemyHeroCardInfluence = new List<string>();
        enemyHeroCardDescription = new List<string>();
        enemyHeroCardPowerLevel = new List<string>();
        enemyHeroCardEffect = new List<string>();
        enemyHeroCardID = new List<string>();

        //DLC Hero Cards
        heroDLCCardName = new List<string>();
        heroDLCCardInfluence = new List<string>();
        heroDLCCardDescription = new List<string>();
        heroDLCCardPowerLevel = new List<string>();
        heroDLCCardEffect = new List<string>();
        heroDLCCardID = new List<string>();

        string path = "NormalCards";
        string fileContents = Resources.Load<TextAsset>(path).text;

        var lines = fileContents.Split("\n"[0]);
        foreach (var line in lines)
        {
            string[] splitArray = line.Split(';');
            normalCardID.Add(splitArray[0]);
            normalCardName.Add(splitArray[1]);
            normalCardInfluence.Add(splitArray[2]);
            normalCardDescription.Add(splitArray[3]);
            normalCardPowerLevel.Add(splitArray[4]);
            normalCardEffect.Add(splitArray[5]);
        }

        path = "EnemyNormalCards";
        fileContents = Resources.Load<TextAsset>(path).text;

        lines = fileContents.Split("\n"[0]);
        foreach (var line in lines)
        {
            string[] splitArray = line.Split(';');
            normalEnemyCardID.Add(splitArray[0]);
            normalEnemyCardName.Add(splitArray[1]);
            normalEnemyCardInfluence.Add(splitArray[2]);
            normalEnemyCardDescription.Add(splitArray[3]);
            normalEnemyCardPowerLevel.Add(splitArray[4]);
            normalEnemyCardEffect.Add(splitArray[5]);
        }

        path = "HeroCards";
        fileContents = Resources.Load<TextAsset>(path).text;

        lines = fileContents.Split("\n"[0]);
        foreach (var line in lines)
        {
            string[] splitArray = line.Split(';');
            heroCardID.Add(splitArray[0]);
            heroCardName.Add(splitArray[1]);
            heroCardInfluence.Add(splitArray[2]);
            heroCardDescription.Add(splitArray[3]);
            heroCardPowerLevel.Add(splitArray[4]);
            heroCardEffect.Add(splitArray[5]);
        }

        path = "EnemyHeroCards";
        fileContents = Resources.Load<TextAsset>(path).text;

        lines = fileContents.Split("\n"[0]);
        foreach (var line in lines)
        {
            string[] splitArray = line.Split(';');
            enemyHeroCardID.Add(splitArray[0]);
            enemyHeroCardName.Add(splitArray[1]);
            enemyHeroCardInfluence.Add(splitArray[2]);
            enemyHeroCardDescription.Add(splitArray[3]);
            enemyHeroCardPowerLevel.Add(splitArray[4]);
            enemyHeroCardEffect.Add(splitArray[5]);
        }

        path = "DLCHeroCards";
        if (File.Exists(Application.streamingAssetsPath +"/" +path + ".txt"))
        {
            Debug.Log("DLC IS DOWNLOADED");
            hasDLC = true;
            fileContents = File.ReadAllText(Application.streamingAssetsPath + "/" + path + ".txt");

            lines = fileContents.Split("\n"[0]);
            foreach (var line in lines)
            {
                string[] splitArray = line.Split(';');
                if (splitArray.Length > 1)
                {
                    heroDLCCardID.Add(splitArray[0]);
                    heroDLCCardName.Add(splitArray[1]);
                    heroDLCCardInfluence.Add(splitArray[2]);
                    heroDLCCardDescription.Add(splitArray[3]);
                    heroDLCCardPowerLevel.Add(splitArray[4]);
                    heroDLCCardEffect.Add(splitArray[5]);
                }
            }

        }
        else
        {
            Debug.Log("DLC NOT DOWNLOADED");
            hasDLC = false;
        }

    }
}
