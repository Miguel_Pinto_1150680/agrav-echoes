﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StageMapController : MonoBehaviour
{

    public EchoesControllerScript echoesController;
    public Button[] startButtons;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(echoesController.ActiveDeck.Count < 20)
        {
            foreach(Button button in startButtons)
            {
                button.interactable = false;
            }
        }
        else
        {
            foreach (Button button in startButtons)
            {
                button.interactable = true;
            }
        }
    }
}
