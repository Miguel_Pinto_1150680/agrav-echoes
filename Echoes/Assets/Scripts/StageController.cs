﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageController : MonoBehaviour
{
    public TextAsset stageSpecification;
    public TextAsset stage2Specification;
    public TextAsset stage3Specification;
    public EchoesControllerScript echoesController;
    public int selectedID = 0;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SetSelected(int id)
    {
        selectedID = id;
    }

    public void SetupEnemyDeck()
    {
        switch (selectedID)
        {
            case 0:
                ReadEnemyDeck(stageSpecification);
                break;
            case 1:
                ReadEnemyDeck(stage2Specification);
                break;
            case 2:
                ReadEnemyDeck(stage3Specification);
                break;
        }
    }

    public void ReadEnemyDeck(TextAsset textAsset)
    {
        string[] splitArray = textAsset.text.Split(';');
        echoesController.ActiveEnemyDeck.Clear();
        foreach (string cardID in splitArray)
        {
            int id = int.Parse(cardID);
            foreach(Card card in echoesController.EnemyCards)
            {
                if(card.id == id)
                {
                    echoesController.ActiveEnemyDeck.Add(new Card(card.id, card.name, card.description, card.influence, card.image, card.powerLevel, card.effect));
                    break;
                }
            }
        }

        Debug.Log(echoesController.ActiveEnemyDeck);
    }
}

