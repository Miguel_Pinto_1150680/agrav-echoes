﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameControlScript : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject[] PlayerCardSpaces;
    public GameObject[] EnemyCardSpaces;
    public Card CardToBePlayed;
    public GameObject[] PlayerHandGameObjects;
    public List<Card> playerHand;
    public List<Card> playerDeck;
    public bool isEnemyTurn = false;
    public bool PlaceMode = false;
    public GameObject WarningPanel;
    public GameObject EndTurnButton;
    private bool isEndOfTurn = false;
    public GameObject DuelInfo;
    public int turns = 20;
    public EchoesControllerScript echoesController;
    public GameObject ScoreCanvas;
    public GameObject GameCanvas;

    private void OnEnable()
    {
        ResetGame();
    }
    void Start()
    {

    }

    public void ResetGame()
    {
        ClearField();
        ToggleControls(true);
        turns = 20;
        SetupPlayerHand();
        GetComponent<EnemyController>().SetupEnemy();
        UpdateDuelInfo();
    }

    public void ClearField()
    {
        for (int i = 0; i < 3; i++)
        {
            EnemyCardSpaces[i].GetComponent<CardFrameController>().Card = null;
            EnemyCardSpaces[i].GetComponent<CardFrameController>().FramedCardObject.SetActive(false);
            PlayerCardSpaces[i].GetComponent<CardFrameController>().Card = null;
            PlayerCardSpaces[i].GetComponent<CardFrameController>().FramedCardObject.SetActive(false);
        }
    }

    public void DoEnemyTurn()
    {
        ToggleControls(false);
        GetComponent<EnemyController>().DoTurn();
    }

    public void UpdateDuelInfo()
    {
        int influence = GetComponent<EnemyController>().EnemyInfluence;
        if (influence < 0)
        {
            influence = 0;
        }
        DuelInfo.GetComponent<Text>().text = string.Format("Enemy Influence: {0}\nTurns Left: {1}", influence, turns);
    }

    public void SetupPlayerHand()
    {
        List<Card> ActiveDeck = new List<Card>(echoesController.ActiveDeck);
        playerDeck = new List<Card>();
        foreach (Card card in ActiveDeck)
        {
            playerDeck.Add(new Card(card.id, card.name, card.description, card.influence, card.image, card.powerLevel, card.effect));
        }
        Debug.Log("ACTIVE DECK SIZE " + playerDeck.Count);
        playerHand = new List<Card>();

        for (int i = 0; i < 3; i++)
        {
            DrawCard();
        }

        DrawCard();
        UpdatePlayerHand();

    }

    public void DrawCard()
    {
        if (playerDeck.Count <= 0)
        {
            return;
        }
        int CardIndex = Random.Range(0, playerDeck.Count);
        Card DrawnCard = playerDeck[CardIndex];
        playerHand.Add(DrawnCard);
        playerDeck.RemoveAt(CardIndex);
        Debug.Log("Player Deck Size " + playerDeck.Count);
    }

    public void UpdatePlayerHand()
    {
        for (int i = 0; i < PlayerHandGameObjects.Length; i++)
        {
            PlayableCardScript playableCardScript = PlayerHandGameObjects[i].GetComponent<PlayableCardScript>();
            if (playableCardScript.bIsSelected)
            {
                PlayerHandGameObjects[i].GetComponent<PlayableCardScript>().UnselectSelf();
            }

            if (i < playerHand.Count && playerHand[i] != null)
            {
                PlayerHandGameObjects[i].SetActive(true);
                PlayerHandGameObjects[i].GetComponent<PlayableCardScript>().card = playerHand[i];
                PlayerHandGameObjects[i].GetComponent<CardInfoScript>().UpdateCard(playerHand[i]);
            }
            else
            {
                PlayerHandGameObjects[i].GetComponent<PlayableCardScript>().card = null;
                PlayerHandGameObjects[i].SetActive(false);
            }
        }

        if (isEndOfTurn)
        {
            if (playerHand.Count <= 5)
            {
                EndTurnButton.GetComponent<Button>().interactable = true;
            }
        }
    }

    public void RemoveCardFromHand(Card card)
    {
        playerHand.Remove(card);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void TogglePlaceMode()
    {
        PlaceMode = !PlaceMode;
    }

    public void TogglePlaceMode(bool value)
    {
        PlaceMode = value;
    }

    public IEnumerator DoCardBattleWithPause(int seconds, List<Card> playerCards, List<Card> enemyCards, int position)
    {
        // doing something

        // waits 2 seconds



        int i = position;
        Card playerCard = playerCards[i];
        Card enemyCard = enemyCards[i];

        if (playerCard != null)
        {
            yield return new WaitForSeconds(seconds);
            Debug.Log("STARTED BATTLE " + position);
        }
        else
        {
            yield return new WaitForSeconds(0);
            Debug.Log("SKIPPED BATTLE");
        }

        if (playerCard != null && enemyCard != null)
        {
            int playerCardInitialInfluence = playerCard.influence;
            int enemyCardInitialInfluence = enemyCard.influence;

            Debug.Log(string.Format("Player used card {0} with {1} influence.\n Enemy used card {2} with {3} influence.", playerCard.name, playerCard.influence, enemyCard.name, enemyCard.influence));
            playerCard.influence -= enemyCardInitialInfluence;
            enemyCard.influence -= playerCardInitialInfluence;


            if (enemyCard.influence <= 0)
            {
                EnemyCardSpaces[i].GetComponent<CardFrameController>().Card = null;
                EnemyCardSpaces[i].GetComponent<CardFrameController>().FramedCardObject.SetActive(false);
            }
            else
            {
                EnemyCardSpaces[i].GetComponent<CardFrameController>().Card = enemyCard;
                EnemyCardSpaces[i].GetComponent<CardFrameController>().FramedCardObject.GetComponent<CardInfoScript>().UpdateCard(enemyCard);
            }

            if (playerCard.influence <= 0)
            {
                PlayerCardSpaces[i].GetComponent<CardFrameController>().Card = null;
                PlayerCardSpaces[i].GetComponent<CardFrameController>().FramedCardObject.SetActive(false);
            }
            else
            {
                PlayerCardSpaces[i].GetComponent<CardFrameController>().Card = playerCard;
                PlayerCardSpaces[i].GetComponent<CardFrameController>().FramedCardObject.GetComponent<CardInfoScript>().UpdateCard(playerCard);
            }
        }
        else if (playerCard != null && enemyCard == null)
        {
            GetComponent<EnemyController>().EnemyInfluence -= playerCard.influence;
            UpdateDuelInfo();
        }

        if (position < 2)
        {
            StartCoroutine(DoCardBattleWithPause(seconds, playerCards, enemyCards, i + 1));
        }
        else
        {
            FinishTurnSet();
        }

    }

    public void FinishTurnSet()
    {
        turns--;
        UpdateDuelInfo();

        if (!CheckGameOverConditions())
        {
            ToggleControls(true);
            CheckTurnStartEffects(PlayerCardSpaces);
            ValidateFieldedCards();
            DrawCard();
            UpdatePlayerHand();
        }
    }

    public IEnumerator DoBattlePhase()
    {
        Debug.Log("STARTED BATTLE PHASE");
        List<Card> FieldedPlayerCards = new List<Card>();

        foreach (GameObject cardFrame in PlayerCardSpaces)
        {
            Card card = cardFrame.GetComponent<CardFrameController>().Card;
            if (card != null)
            {
                FieldedPlayerCards.Add(card);
            }
            else
            {
                FieldedPlayerCards.Add(null);
            }
        }

        List<Card> FieldedEnemyCards = new List<Card>();

        foreach (GameObject cardFrame in EnemyCardSpaces)
        {
            Card card = cardFrame.GetComponent<CardFrameController>().Card;
            if (card != null)
            {
                FieldedEnemyCards.Add(card);
            }
            else
            {
                FieldedEnemyCards.Add(null);
            }
        }

        yield return StartCoroutine(DoCardBattleWithPause(2, FieldedPlayerCards, FieldedEnemyCards, 0));
    }


    public void CheckTurnEndEffects(GameObject[] cardSpaces)
    {
        int pos = 0;
        foreach (GameObject cardFrame in cardSpaces)
        {
            Card card = cardFrame.GetComponent<CardFrameController>().Card;
            if (card != null && card.effect != null && card.effect.isTriggeredOnTurnEnd())
            {
                string[] args = { string.Format("{0}", pos) };
                card.effect.ApplyEffect(args);
            }
            pos++;
        }
    }

    public void CheckTurnStartEffects(GameObject[] cardSpaces)
    {
        foreach (GameObject cardFrame in cardSpaces)
        {
            Card card = cardFrame.GetComponent<CardFrameController>().Card;
            if (card != null && card.effect != null && card.effect.isTriggeredOnTurnStart())
            {
                string[] args = { };
                card.effect.ApplyEffect(args);
            }
        }
    }

    public void EndTurn()
    {
        if (!CheckGameOverConditions())
        {
            if (playerHand.Count > 5)
            {
                WarningPanel.SetActive(true);
                EndTurnButton.GetComponent<Button>().interactable = false;
                isEndOfTurn = true;
                return;
            }
            isEndOfTurn = false;
            WarningPanel.SetActive(false);
            CheckTurnEndEffects(PlayerCardSpaces);
            ValidateFieldedCards();
            DoEnemyTurn();
        }
    }


    public void ToggleControls(bool value)
    {
        foreach (GameObject gameObject in PlayerHandGameObjects)
        {
            gameObject.GetComponent<Button>().interactable = value;
            EndTurnButton.GetComponent<Button>().interactable = value;
        }
    }

    public void ShowScore(int score)
    {
        ScoreCanvas.SetActive(true);
        ScoreCanvas.GetComponent<StarController>().SetScore(score);
        GameCanvas.SetActive(false);
    }

    public bool CheckGameOverConditions()
    {

        if (GetComponent<EnemyController>().EnemyInfluence < 1)
        {
            if (turns > 10)
            {
                Debug.Log("GAME OVER 3*");
                ShowScore(3);
                return true;
            }
            else if (turns > 5)
            {
                Debug.Log("GAME OVER 2*");
                ShowScore(2);
                return true;
            }
            else
            {
                Debug.Log("GAME OVER 1*");
                ShowScore(1);
                return true;
            }
        }


        int fieldedCards = 0;

        for (int i = 0; i < 3; i++)
        {
            if (PlayerCardSpaces[i].GetComponent<CardFrameController>().Card != null)
            {
                fieldedCards++;
            }
        }

        if ((playerHand.Count < 1 && playerDeck.Count < 1 && fieldedCards < 1) || turns < 1)
        {
            Debug.Log("GAME OVER 0*");
            ShowScore(0);
            return true;
        }

        return false;
    }


    public void ValidateFieldedCards()
    {
        List<Card> FieldedPlayerCards = new List<Card>();

        foreach (GameObject cardFrame in PlayerCardSpaces)
        {
            Card card = cardFrame.GetComponent<CardFrameController>().Card;
            if (card != null)
            {
                FieldedPlayerCards.Add(card);
            }
            else
            {
                FieldedPlayerCards.Add(null);
            }
        }

        List<Card> FieldedEnemyCards = new List<Card>();

        foreach (GameObject cardFrame in EnemyCardSpaces)
        {
            Card card = cardFrame.GetComponent<CardFrameController>().Card;
            if (card != null)
            {
                FieldedEnemyCards.Add(card);
            }
            else
            {
                FieldedEnemyCards.Add(null);
            }
        }

        for (int i = 0; i < 3; i++)
        {
            Card playerCard = FieldedPlayerCards[i];
            Card enemyCard = FieldedEnemyCards[i];

            if (enemyCard != null && enemyCard.influence <= 0)
            {
                EnemyCardSpaces[i].GetComponent<CardFrameController>().Card = null;
                EnemyCardSpaces[i].GetComponent<CardFrameController>().FramedCardObject.SetActive(false);
            }

            if (playerCard != null && playerCard.influence <= 0)
            {
                PlayerCardSpaces[i].GetComponent<CardFrameController>().Card = null;
                PlayerCardSpaces[i].GetComponent<CardFrameController>().FramedCardObject.SetActive(false);
            }
        }


    }

}
