﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour

{
    public List<Card> EnemyHand;
    public List<Card> EnemyDeck;
    public GameControlScript GameController;
    public EchoesControllerScript EchoesController;
    public DDAController ddaController;
    public int EnemyInfluence;
    // Start is called before the first frame update
    void Start()
    {
    }

    public void SetupEnemy()
    {
        EnemyDeck = new List<Card>();
        EnemyHand = new List<Card>();
        ddaController = new DDAController(GameController, this);
        EnemyInfluence = 35;
        List<Card> ActiveDeck = EchoesController.ActiveEnemyDeck;
        foreach (Card card in ActiveDeck)
        {
            EnemyDeck.Add(new Card(card.id, card.name, card.description, card.influence, card.image, card.powerLevel, card.effect));
        }
        Debug.Log("ACTIVE DECK SIZE " + EnemyDeck.Count);

        for (int i = 0; i < 3; i++)
        {
            DrawCard();
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public IEnumerator DoCardPlacementWithPause(int seconds, List<int> AvailableSpaces)
    {
        // doing something

        // waits 2 seconds

        yield return new WaitForSeconds(seconds);
        if (AvailableSpaces.Count == 0)
        {

            EndTurn();
        }
        else
        {
            List<Card> HumanCards = IdentifyPlayerCards();
            PlayCard();
            EndTurn();
        }
    }

    public void DoTurn()
    {
        GameController.isEnemyTurn = true;
        GameController.CheckTurnStartEffects(GameController.EnemyCardSpaces);
        GameController.ValidateFieldedCards();
        DrawCard();
        List<int> AvailableSpaces = CheckAvailableSpaces();
        StartCoroutine(DoCardPlacementWithPause(2, AvailableSpaces));


    }

    public void DrawCard()
    {

        if (EnemyDeck.Count <= 0)
        {
            return;
        }
        int CardIndex = Random.Range(0, EnemyDeck.Count);
        Card DrawnCard = EnemyDeck[CardIndex];
        EnemyHand.Add(DrawnCard);
        EnemyDeck.RemoveAt(CardIndex);
        Debug.Log("Enemy Deck Size " + EnemyDeck.Count);
    }

    public void PlayCard()
    {
        //TODO CHOSE BETTER SPOT AND BETTER CARDS TO PLAY

        (Card,int) play = ddaController.DeterminePlay();
        Card card = play.Item1;
        if(card == null)
        {
            EndTurn();
        }
        int pos = play.Item2;
        EnemyHand.Remove(card); 
        if(card == null)
        {
            return;
        }
        GameController.EnemyCardSpaces[pos].GetComponent<CardFrameController>().Card = card;
        GameController.EnemyCardSpaces[pos].GetComponent<CardFrameController>().FramedCardObject.SetActive(true);
        GameController.EnemyCardSpaces[pos].GetComponent<CardFrameController>().FramedCardObject.GetComponent<CardInfoScript>().UpdateCard(card.name, card.description, card.influence, card.image, card.powerLevel);
        if (card.effect != null && card.effect.isTriggeredOnPlacement())
        {
            string[] args = { string.Format("{0}", pos) };
            GameController.isEnemyTurn = true;
            card.effect.ApplyEffect(args);
            GameController.ValidateFieldedCards();
        }
    }

    public List<int> CheckAvailableSpaces()
    {
        List<int> Result = new List<int>();
        for (int i = 0; i < GameController.EnemyCardSpaces.Length; i++)
        {
            Card card = GameController.EnemyCardSpaces[i].GetComponent<CardFrameController>().Card;
            if (card == null)
            {
                Result.Add(i);
            }
        }
        return Result;


    }

    public List<Card> IdentifyPlayerCards()
    {
        List<Card> Result = new List<Card>();

        foreach (GameObject cardFrame in GameController.PlayerCardSpaces)
        {
            Card card = cardFrame.GetComponent<CardFrameController>().Card;
            if (card != null)
            {
                Result.Add(card);
            }
            else
            {
                Result.Add(new Card(0, "EMPTY", "NO CARD", -1, "cards/noCard", -1, null));
            }
        }
        return Result;
    }

    public void EndTurn()
    {
        GameController.isEnemyTurn = true;
        GameController.CheckTurnEndEffects(GameController.EnemyCardSpaces);
        GameController.isEnemyTurn = false;
        StartCoroutine(GameController.DoBattlePhase());
    }


}
