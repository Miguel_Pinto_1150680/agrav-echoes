﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using FullSerializer;
using System.Text.RegularExpressions;
using Proyecto26;

public class StarController : MonoBehaviour
{
    public Image[] stars = new Image[3];
    public EchoesControllerScript echoesScript;
    public ReadCardFile readCardFileScript;
    private SignIn signInScript;
    public Text title;
    private User newUSer;
    private string databaseURL = "https://agrav-echoes-default-rtdb.europe-west1.firebasedatabase.app/users"; 
    // private string AuthKey = "secret ;D";
    private string AuthKey = "AIzaSyB0papksovEijhaIV78KZk7gURclBOHXgc";

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetScore(int score)
    {
        if(score > 0)
        {
            title.text = "VICTORY";
            ColourStars(score);
        }
        else
        {
            title.text = "DEFEAT";
            ColourStars(0);
        }
        UnlockCard(score);

        
    }

    public void ColourStars(int score)
    {
        for(int i = 0; i< 3; i++)
        {
            if(i < score)
            {
                stars[i].color = Color.yellow;
            }
            else
            {
                stars[i].color = Color.white;
            }
        }
    }

    public void UnlockCard(int score)
    {
        SignIn[] signInScriptList = Resources.FindObjectsOfTypeAll<SignIn>();
        signInScript = signInScriptList[0];
        string stages = signInScript.userLogged.userStage;
        newUSer = signInScript.userLogged;
        int idCard = 0;

        if(score > 2)
        {
            // EchoesControllerScript[] echoesScriptList = Resources.FindObjectsOfTypeAll<EchoesControllerScript>();
            // echoesScript = echoesScriptList[0];

            for(int i = 0; i< readCardFileScript.enemyHeroCardID.Count; i++)
            {
                for(int f = 0; f< echoesScript.ActiveEnemyDeck.Count; f++)
                {
                    if (readCardFileScript.enemyHeroCardID[i].Equals(echoesScript.ActiveEnemyDeck[f].id.ToString()))
                    {
                        idCard = echoesScript.ActiveEnemyDeck[f].id -70;
                        goto End;
                    }
                }
            }

            End:char[] chars = stages.ToCharArray();
                chars[idCard] = '1';
                stages = new string(chars);

                newUSer.userStage = stages;
                SaveStage();

        }
    }

    public void SaveStage(){

         RestClient.Put<User>(databaseURL + "/" + signInScript.getLocalId + ".json?auth=" + signInScript.idToken,newUSer).Then(response =>
            {
                Debug.Log(signInScript.userLogged.userEmail);
                Debug.Log(signInScript.userLogged.userStage);
                Debug.Log("UPDATED");
                echoesScript.UpdateCardCollection();
            });
            
    }

}
