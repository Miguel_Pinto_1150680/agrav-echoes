﻿using System.Collections.Generic;
using UnityEngine;

public class DDAController
{
    private GameControlScript gameController;
    private EnemyController enemyController;

    private int EI = 3;

    public DDAController(GameControlScript gameController, EnemyController enemyController)
    {
        this.gameController = gameController;
        this.enemyController = enemyController;
    }


    public (Card, int) DeterminePlay()
    {
        List<(Card, Card)> fieldedCards = GetFieldedCards();
        int influenceChange = MaxInfluenceChange(fieldedCards);
        if (influenceChange > MeanInfluenceChange())
        {
            ChangeDifficulty(1);
        }
        else
        {
            ChangeDifficulty(-1);
        }

        Card card = null;
        int position = -1;

        (Card, int) bestPlayerCard = GetStrongestUnopposedPlayerCard(fieldedCards);
        if(bestPlayerCard.Item1 == null)
        {
            bestPlayerCard = GetStrongestPlayerCard(fieldedCards);
            List<int> AvailableSpaces = CheckAvailableSpaces();
            int i = Random.Range(0, AvailableSpaces.Count);
            position = AvailableSpaces[i];

        }
        if (bestPlayerCard.Item2 != -1)
        {
            position = bestPlayerCard.Item2;
            int tempEI = EI;
            while(tempEI > 0 && card == null)
            {
                card = getCardWithEI(tempEI, bestPlayerCard.Item1.influence);
                tempEI--;
            }
            return (card, position);
        }
        else
        {
            List<int> AvailableSpaces = CheckAvailableSpaces();
            int i = Random.Range(0, AvailableSpaces.Count);
            int pos = AvailableSpaces[i];
            i = Random.Range(0, enemyController.EnemyHand.Count);
            card = enemyController.EnemyHand[i];
            return (card, pos);
        }
    }

    public Card getCardWithEI(int ei, int influence)
    {
        Card card = null;

        switch (ei)
        {
            case 1:
                card = getCardWithInfluence(0, influence - 1);
                break;
            case 2:
                card = getCardWithInfluence(0, influence);
                break;
            case 3:
                card = getCardWithInfluence(influence, influence);
                break;
            case 4:
                card = getCardWithInfluence(influence, int.MaxValue);
                break;
            case 5:
                card = getCardWithInfluence(influence + 1, int.MaxValue);
                break;
        }

        return card;
    }

    public Card getCardWithInfluence(int min, int max)
    {
        Card result = null;
        if(max == 0)
        {
            max = 1;
        }
        foreach(Card card in enemyController.EnemyHand)
        {
            if(card.influence >= min && card.influence <= max)
            {
                result = card;
                break;
            }
        }

        return result;
    }

    public void ChangeDifficulty(int delta)
    {
        if (EI != 6 && EI != 1)
        {
            EI += delta;
        }
    }

    public float MeanInfluenceChange()
    {
        if (gameController.turns > 17)
        {
            return 2.0f;
        }
        else
        {
            int totalChange = 50 - enemyController.EnemyInfluence;
            int turns = 20 - gameController.turns;
            return totalChange / turns;
        }
    }

    public float AverageCardCollectionPower(List<Card> deck)
    {
        int totalPower = 0;
        int deckSize = deck.Count;

        foreach (Card card in deck)
        {
            totalPower += card.powerLevel;
        }

        return totalPower / deckSize;
    }

    public int MaxInfluenceChange(List<(Card, Card)> fieldedCards)
    {
        int influenceChange = 0;

        foreach ((Card, Card) cardTuple in fieldedCards)
        {
            if (cardTuple.Item1 != null && cardTuple.Item2 == null)
            {
                if (influenceChange < cardTuple.Item1.influence)
                {
                    influenceChange = cardTuple.Item1.influence;
                }
            }
        }

        return influenceChange;

    }

    public List<int> CheckAvailableSpaces()
    {
        List<int> Result = new List<int>();
        for (int i = 0; i < gameController.EnemyCardSpaces.Length; i++)
        {
            Card card = gameController.EnemyCardSpaces[i].GetComponent<CardFrameController>().Card;
            if (card == null)
            {
                Result.Add(i);
            }
        }
        return Result;


    }

    public (Card, int) GetStrongestUnopposedPlayerCard(List<(Card, Card)> fieldedCards)
    {
        int power = 0;
        int pos = -1;
        int i = -1;
        Card card = null;

        foreach ((Card, Card) cardTuple in fieldedCards)
        {
            i++;
            if (cardTuple.Item1 != null && cardTuple.Item2 == null)
            {
                if (cardTuple.Item1.powerLevel > power)
                {
                    power = cardTuple.Item1.powerLevel;
                    card = cardTuple.Item1;
                    pos = i;
                }
            }
        }

        return (card, pos);

    }

    public (Card, int) GetStrongestPlayerCard(List<(Card, Card)> fieldedCards)
    {
        int power = 0;
        int pos = -1;
        int i = -1;
        Card card = null;

        foreach ((Card, Card) cardTuple in fieldedCards)
        {
            i++;
            if (cardTuple.Item1 != null)
            {
                if (cardTuple.Item1.powerLevel > power)
                {
                    power = cardTuple.Item1.powerLevel;
                    card = cardTuple.Item1;
                    pos = i;
                }
            }
        }

        return (card, pos);

    }

    public List<(Card, Card)> GetFieldedCards()
    {
        List<(Card, Card)> fieldedCards = new List<(Card, Card)>();
        for (int i = 0; i < 3; i++)
        {
            Card playerCard = gameController.PlayerCardSpaces[i].GetComponent<CardFrameController>().Card;
            Card enemyCard = gameController.EnemyCardSpaces[i].GetComponent<CardFrameController>().Card;

            fieldedCards.Add((playerCard, enemyCard));
        }

        return fieldedCards;

    }


}