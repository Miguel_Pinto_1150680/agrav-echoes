﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(UnityEngine.UIElements.Image))]
public class CardFrameController : MonoBehaviour
{
    // Start is called before the first frame update
    public Card Card = null;
    public GameControlScript GameController;
    public GameObject FramedCardObject;
    Image FrameSprite;
    Button FrameButton;


    void Start()
    {
        FrameSprite = GetComponent<Image>();
        FrameButton = GetComponent<Button>();
        FrameButton.interactable = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameController.PlaceMode)
        {
            foreach (GameObject CardFrame in GameController.PlayerCardSpaces)
            {
                CardFrame.GetComponent<CardFrameController>().CheckAvailable();
            }

        }
        else
        {
            if (FrameSprite.color != Color.white)
            {
                ResetFrame();
            }
        }

    }

    void ResetFrame()
    {
        FrameSprite.color = Color.white;
        FrameButton.interactable = false;
    }

    void CheckAvailable()
    {

        if (Card == null)
        {
            FrameSprite.color = Color.green;
            FrameButton.interactable = true;
            //Debug.Log("GREEN");
        }
        else
        {
            FrameSprite.color = Color.red;
            FrameButton.interactable = false;
            //Debug.Log("RED");
        }
    }

    public void OnClick()
    {
        CardInfoScript fieldCardScript = FramedCardObject.GetComponent<CardInfoScript>();
        Card = GameController.CardToBePlayed;
        GameController.TogglePlaceMode(false);
        int cardPosition = -1;
        for(int i = 0; i < GameController.PlayerCardSpaces.Length; i++)
        {
            if(GameController.PlayerCardSpaces[i] == gameObject)
            {
                cardPosition = i;
            }
        }
        if (Card != null)
        {
            FramedCardObject.SetActive(true);
            fieldCardScript.UpdateCard(Card);
            if (Card.effect != null && Card.effect.isTriggeredOnPlacement())
            {
                string[] args = {string.Format("{0}", cardPosition) };
                Card.effect.ApplyEffect(args);
            }
            GameController.RemoveCardFromHand(Card);
            GameController.UpdateDuelInfo();
            GameController.UpdatePlayerHand();
            
        }
        GameController.EndTurn();
    }






}
