﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModifyEventInfluence : CardEffect
{
    int amount = 0;
    public ModifyEventInfluence(int amount)
    {
        this.amount = amount;
    }


    public void ApplyEffect(string[] args)
    {
        GameObject gcGameObject = GameObject.FindGameObjectWithTag("GameController");
        if (gcGameObject == null)
        {
            return;
        }
        GameControlScript gameController = gcGameObject.GetComponent<GameControlScript>();
        if (gameController == null)
        {
            return;
        }

        gameController.GetComponent<EnemyController>().EnemyInfluence += amount;


    }

    public bool isTriggeredOnPlacement()
    {
        return true;
    }

    public bool isTriggeredOnTurnEnd()
    {
        return false;
    }

    public bool isTriggeredOnTurnStart()
    {
        return false;
    }
}
