﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModifyAdjacentFriendlyCardInfluence : CardEffect
{
    int amount = 0;
    public ModifyAdjacentFriendlyCardInfluence(int amount)
    {
        this.amount = amount;
    }

    public void ApplyEffect(string[] args)
    {
        GameObject gcGameObject = GameObject.FindGameObjectWithTag("GameController");
        if (gcGameObject == null)
        {
            return;
        }
        GameControlScript gameController = gcGameObject.GetComponent<GameControlScript>();
        if (gameController == null)
        {
            return;
        }

        GameObject[] cardSpaces;

        if (gameController.isEnemyTurn)
        {
            cardSpaces = gameController.EnemyCardSpaces;
        }
        else
        {
            cardSpaces = gameController.PlayerCardSpaces;
        }

        int cardPosition = int.Parse(args[0]);
        for (int i = 0; i < cardSpaces.Length; i++)
        {
            if((i-cardPosition == 1 || i-cardPosition == -1) && cardSpaces[i].GetComponent<CardFrameController>().Card != null) { 
            cardSpaces[i].GetComponent<CardFrameController>().Card.influence += amount;
            cardSpaces[i].GetComponent<CardFrameController>().FramedCardObject.GetComponent<CardInfoScript>().UpdateCard(cardSpaces[i].GetComponent<CardFrameController>().Card);
            }
        }
            
        
    }

    public bool isTriggeredOnPlacement()
    {
        return true;
    }

    public bool isTriggeredOnTurnEnd()
    {
        return false;
    }

    public bool isTriggeredOnTurnStart()
    {
        return false;
    }
}
