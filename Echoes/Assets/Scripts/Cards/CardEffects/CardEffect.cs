﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface CardEffect
{
    bool isTriggeredOnTurnStart();
    bool isTriggeredOnTurnEnd();
    bool isTriggeredOnPlacement();

    /**
     * args[0] = Card Position
     */
    void ApplyEffect(string[] args);
}
