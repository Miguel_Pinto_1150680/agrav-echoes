﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModifyAllEnemyCardInfluence : CardEffect
{
    int amount = 0;
    public ModifyAllEnemyCardInfluence(int amount)
    {
        this.amount = amount;
    }

    public void ApplyEffect(string[] args)
    {
        GameObject gcGameObject = GameObject.FindGameObjectWithTag("GameController");
        if (gcGameObject == null)
        {
            return;
        }
        GameControlScript gameController = gcGameObject.GetComponent<GameControlScript>();
        if (gameController == null)
        {
            return;
        }

        GameObject[] cardSpaces;

        if (gameController.isEnemyTurn)
        {
            cardSpaces = gameController.PlayerCardSpaces;
        }
        else
        {
            
               cardSpaces = gameController.EnemyCardSpaces;
        }


        List<int> occupiedPositions = new List<int>();
        for (int i = 0; i < cardSpaces.Length; i++)
        {
            if (cardSpaces[i].GetComponent<CardFrameController>().Card != null)
            {
                occupiedPositions.Add(i);
            }
        }
        if (occupiedPositions.Count == 0)
        {
            return;
        }
        else
        {
            foreach (int cardPosition in occupiedPositions)
            {

                if (cardSpaces[cardPosition].GetComponent<CardFrameController>().Card != null)
                {
                    cardSpaces[cardPosition].GetComponent<CardFrameController>().Card.influence += amount;
                    cardSpaces[cardPosition].GetComponent<CardFrameController>().FramedCardObject.GetComponent<CardInfoScript>().UpdateCard(cardSpaces[cardPosition].GetComponent<CardFrameController>().Card);

                }
            }
        }


    }

    public bool isTriggeredOnPlacement()
    {
        return true;
    }

    public bool isTriggeredOnTurnEnd()
    {
        return false;
    }

    public bool isTriggeredOnTurnStart()
    {
        return false;
    }
}
