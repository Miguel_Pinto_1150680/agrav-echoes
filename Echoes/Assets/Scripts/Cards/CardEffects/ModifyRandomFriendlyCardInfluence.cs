﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModifyRandomFriendlyCardInfluence : CardEffect
{
    int amount = 0;
    public ModifyRandomFriendlyCardInfluence(int amount)
    {
        this.amount = amount;
    }

    public void ApplyEffect(string[] args)
    {
        GameObject gcGameObject = GameObject.FindGameObjectWithTag("GameController");
        if (gcGameObject == null)
        {
            return;
        }
        GameControlScript gameController = gcGameObject.GetComponent<GameControlScript>();
        if (gameController == null)
        {
            return;
        }

        GameObject[] cardSpaces;

        if (gameController.isEnemyTurn)
        {
            cardSpaces = gameController.EnemyCardSpaces;
        }
        else
        {
            cardSpaces = gameController.PlayerCardSpaces;
        }


        List<int> occupiedPositions = new List<int>();
        for (int i = 0; i < cardSpaces.Length; i++)
        {
            if (cardSpaces[i].GetComponent<CardFrameController>().Card != null)
            {
                occupiedPositions.Add(i);
            }
        }
        if (occupiedPositions.Count == 0)
        {
            return;
        }
        else
        {
            int cardPosition = Random.Range(0, occupiedPositions.Count);

            if (cardSpaces[occupiedPositions[cardPosition]].GetComponent<CardFrameController>().Card != null)
            {
                cardSpaces[occupiedPositions[cardPosition]].GetComponent<CardFrameController>().Card.influence += amount;
                cardSpaces[occupiedPositions[cardPosition]].GetComponent<CardFrameController>().FramedCardObject.GetComponent<CardInfoScript>().UpdateCard(cardSpaces[occupiedPositions[cardPosition]].GetComponent<CardFrameController>().Card);

            }
        }


    }

    public bool isTriggeredOnPlacement()
    {
        return true;
    }

    public bool isTriggeredOnTurnEnd()
    {
        return false;
    }

    public bool isTriggeredOnTurnStart()
    {
        return false;
    }
}
