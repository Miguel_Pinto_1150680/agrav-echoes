﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModifySelfInfluence : CardEffect
{
    int amount = 0;
    int cardPosition;
    public ModifySelfInfluence(int amount)
    {
        this.amount = amount;
    }


    public void ApplyEffect(string[] args)
    {
        GameObject gcGameObject = GameObject.FindGameObjectWithTag("GameController");
        if (gcGameObject == null)
        {
            return;
        }
        GameControlScript gameController = gcGameObject.GetComponent<GameControlScript>();
        if (gameController == null)
        {
            return;
        }

        GameObject[] cardSpaces;

        if (gameController.isEnemyTurn)
        {
            cardSpaces = gameController.EnemyCardSpaces;
        }
        else
        {
            cardSpaces = gameController.PlayerCardSpaces;
        }

        int cardPosition = int.Parse(args[0]);

        cardSpaces[cardPosition].GetComponent<CardFrameController>().Card.influence += amount;
        cardSpaces[cardPosition].GetComponent<CardFrameController>().FramedCardObject.GetComponent<CardInfoScript>().UpdateCard(cardSpaces[cardPosition].GetComponent<CardFrameController>().Card);


    }

    public bool isTriggeredOnPlacement()
    {
        return false;
    }

    public bool isTriggeredOnTurnEnd()
    {
        return true;
    }

    public bool isTriggeredOnTurnStart()
    {
        return false;
    }
}
