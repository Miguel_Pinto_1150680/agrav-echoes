﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawCardsEffect : CardEffect
{
    int amount = 0;
    public DrawCardsEffect(int amount)
    {
        this.amount = amount;
    }

    public void ApplyEffect(string[] args)
    {
        GameObject gcGameObject = GameObject.FindGameObjectWithTag("GameController");
        if (gcGameObject == null)
        {
            return;
        }
        GameControlScript gameController = gcGameObject.GetComponent<GameControlScript>();
        if (gameController == null)
        {
            return;
        }

        if (gameController.isEnemyTurn)
        {
            EnemyController enemyController = gcGameObject.GetComponent<EnemyController>();
            for (int i = 0; i < amount; i++)
            {
                enemyController.DrawCard();
            }
        }
        else
        {
            for (int i = 0; i < amount; i++)
            {
                gameController.DrawCard();
                gameController.UpdatePlayerHand();
            }
        }
    }

    public bool isTriggeredOnPlacement()
    {
        return true;
    }

    public bool isTriggeredOnTurnEnd()
    {
        return false;
    }

    public bool isTriggeredOnTurnStart()
    {
        return false;
    }
}
