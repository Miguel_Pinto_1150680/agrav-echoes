﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModifyAdjacentEnemyCard : CardEffect
{
    int amount = 0;
    public ModifyAdjacentEnemyCard(int amount)
    {
        this.amount = amount;
    }

    public void ApplyEffect(string[] args)
    {
        GameObject gcGameObject = GameObject.FindGameObjectWithTag("GameController");
        if (gcGameObject == null)
        {
            return;
        }
        GameControlScript gameController = gcGameObject.GetComponent<GameControlScript>();
        if (gameController == null)
        {
            return;
        }

        GameObject[] cardSpaces;

        if (gameController.isEnemyTurn)
        {
            cardSpaces = gameController.PlayerCardSpaces;
        }
        else
        {
            cardSpaces = gameController.EnemyCardSpaces;
        }
        int cardPosition = int.Parse(args[0]);
        if (cardSpaces[cardPosition].GetComponent<CardFrameController>().Card != null)
        {
            cardSpaces[cardPosition].GetComponent<CardFrameController>().Card.influence += amount;
            cardSpaces[cardPosition].GetComponent<CardFrameController>().FramedCardObject.GetComponent<CardInfoScript>().UpdateCard(cardSpaces[cardPosition].GetComponent<CardFrameController>().Card);

        }


    }

    public bool isTriggeredOnPlacement()
    {
        return true;
    }

    public bool isTriggeredOnTurnEnd()
    {
        return false;
    }

    public bool isTriggeredOnTurnStart()
    {
        return false;
    }
}
