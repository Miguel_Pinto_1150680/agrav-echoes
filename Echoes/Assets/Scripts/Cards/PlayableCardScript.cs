﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayableCardScript : MonoBehaviour
{
    // Start is called before the first frame update
    int iOGSiblingIndex;
    public bool bIsSelected;
    float fMoveAmount;
    public GameObject buttons;
    public GameControlScript GameController;
    public Card card;
    void Start()
    {
        iOGSiblingIndex = this.transform.GetSiblingIndex();
        fMoveAmount = this.GetComponent<BoxCollider2D>().size.y / 10;
        bIsSelected = false;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SelectCardFromHand()
    {
        if (bIsSelected)
        {
            UnselectSelf();       
        }
        else
        {
            UnselectSiblings();
            transform.Translate(new Vector3(0, fMoveAmount, 0));
            transform.SetAsLastSibling();
            bIsSelected = true;
            GameController.CardToBePlayed = card;
            buttons.SetActive(true);
        }
    }

    public void UnselectSelf()
    {
        transform.Translate(new Vector3(0, -fMoveAmount, 0));
            transform.SetSiblingIndex(iOGSiblingIndex);
            bIsSelected = false;
            buttons.SetActive(false);
            GameController.CardToBePlayed = null;
            GameController.TogglePlaceMode(false);
    }

    private void UnselectSiblings()
    {
        PlayableCardScript[] list = transform.parent.GetComponentsInChildren<PlayableCardScript>();
        for (int i = 0; i < list.Length; i++)
        {
            if (list[i] != this && list[i].bIsSelected)
            {
                list[i].transform.Translate(new Vector3(0, -fMoveAmount, 0));
                list[i].transform.SetSiblingIndex(list[i].iOGSiblingIndex);
                list[i].bIsSelected = false;
                GameController.CardToBePlayed = null;
                list[i].buttons.SetActive(false);
            }
        }
    }

    public void DiscardSelf()
    {
        GameController.RemoveCardFromHand(card);
        GameController.UpdatePlayerHand();
    }

}
