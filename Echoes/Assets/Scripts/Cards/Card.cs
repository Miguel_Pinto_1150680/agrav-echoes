﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Card
{
    public string name = "STRING_NAME";
    public string description = "STRING_DESCRIPTION";
    public Sprite image = Resources.Load<Sprite>("cards/noCard");
    public int influence = 0;
    public int powerLevel = 0;
    public int id = -1;
    public CardEffect effect;

    public Card(int id, string name, string description, int influence, string imagePath, int powerLevel, CardEffect effect)
    {
        this.id = id;
        this.name = name;
        this.description = description;
        this.influence = influence;
        this.image = Resources.Load<Sprite>(imagePath);
        this.powerLevel = powerLevel;
        this.effect = effect;
    }

    public Card(int id,string name, string description, int influence, Sprite image, int powerLevel, CardEffect effect)
    {
        this.id = id;
        this.name = name;
        this.description = description;
        this.influence = influence;
        this.image = image;
        this.powerLevel = powerLevel;
        this.effect = effect;
    }

    public Card()
    {
        name = "STRING_NAME";
        description = "STRING_DESCRIPTION";
        image = Resources.Load<Sprite>("cards/noCard");
        influence = 0;
        powerLevel = 0;
        effect = null;
        id = -1;
    }


}
