﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardInfoScript : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject nameTextBox;
    public GameObject descTextBox;
    public GameObject ImagePanel;
    public GameObject InfluenceTextBox;
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UpdateCard(string name, string desc, int influence, Sprite image, int power)
    {   
        nameTextBox.GetComponent<Text>().text = name;
        descTextBox.GetComponent<Text>().text = desc;
        InfluenceTextBox.GetComponent<Text>().text = string.Format("{0}", influence);
        ImagePanel.GetComponent<Image>().sprite = image;

    }
    public void UpdateCard(Card card)
    {
        nameTextBox.GetComponent<Text>().text = card.name;
        descTextBox.GetComponent<Text>().text = card.description;
        InfluenceTextBox.GetComponent<Text>().text = string.Format("{0}", card.influence);
        ImagePanel.GetComponent<Image>().sprite = card.image;
    }
}
